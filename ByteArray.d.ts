export var ByteArray = Uint8Array;

export function fromString(s: string, encoding?: string): Uint8Array;
export function toString(a: Uint8Array, encoding?: string): string;
export function fromGBytes(b: import("./GLib").Bytes): Uint8Array;
export function toGBytes(a: Uint8Array): import("./GLib").Bytes;
