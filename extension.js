'use strict';
/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 * xVan Turing 2019
 */

/* exported init */
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Soup = imports.gi.Soup;
const FileTest = GLib.FileTest;
const St = imports.gi.St;
const ByteArray = imports.byteArray
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const Config = imports.misc.config;
const Utils = Me.imports.Utils;
const fs = Me.imports.fs;
const Share = Me.imports.Share;

const SHELL_MINOR = parseInt(Config.PACKAGE_VERSION.split('.')[1]);
const IndicaterName = "SSR Proxy";
const logger = (msg) => {
    log(`SSR-PROXY: ${msg}`)
}
let SSRIndicator = class extends PanelMenu.Button {
    async onSSRStateChanged(restart = false) {
        let http_port = this.settings.get_int(Share.Names.SSR_HTTP_PORT)
        let http_enabled = this.settings.get_boolean(Share.Names.SSR_ENABLE_HTTP)

        let ssr_enabled = this.settings.get_boolean(Share.Names.SSR_ENABLE)

        let ssr_path = this.settings.get_string(Share.Names.SSR_SCRIPT_PATH)
        let sock_port = this.settings.get_int(Share.Names.SSR_SOCKS_PORT);

        this.ssrStatus.label.set_text(`Shadowsocks: ${ssr_enabled ? "On" : "Off"}`)
        this.ssrStatusToggle.label.set_text(`Turn Shadowsocks ${!ssr_enabled ? "On" : "Off"}`)
        if (ssr_enabled) {
            let ssr_link = this.settings.get_string(Share.Names.SSR_SELECTED)
            logger(`using ssr_link: ${ssr_link}`)
            if (ssr_link === "") {
                Main.notify("SSR Proxy", "You have not selected one server! Tunning Off.")
                this.settings.set_boolean(Share.Names.SSR_ENABLE, false)
                return
            }
            if (ssr_path === "") {
                Main.notify("SSR Proxy", "You have not configured your settings! Tunning Off.")
                this.settings.set_boolean(Share.Names.SSR_ENABLE, false)
                return
            }
            // parse config, add extra key
            let parsedConfig = Utils.parseSSRLink(ssr_link)
            if (parsedConfig == null) {
                Main.notify("SSR Proxy", "Selection is not valid. Tunning Off.")
                this.settings.set_boolean(Share.Names.SSR_ENABLE, false)
                return
            }
            parsedConfig["local_port"] = sock_port
            parsedConfig["local_address"] = this.settings.get_boolean(Share.Names.SSR_SHARE_LAN) ? "0.0.0.0" : "127.0.0.1"
            if (!restart) {
                let pid_content = await fs.readFileFromCache(Share.Names.SSR_PID_FILE_NAME)
                if (pid_content) {
                    let pid = parseInt(pid_content)
                    let is_python_ssr = await Utils.isPythonSSR(pid)
                    if (is_python_ssr) {
                        return // if process exists then return
                    }
                }
            }
            // write config.json
            await fs.writeFileToCache(Share.Names.SSR_CONFIG_FILE_NAME, JSON.stringify(parsedConfig, null, 4))
            let exit_code = 0
            if (restart) {
                exit_code = await Utils.restartSSRDaemon()
            } else {
                exit_code = await Utils.startSSRDaemon()
            }
            if (exit_code !== 0) {
                // something is wrong check pid
            } else {
                if (restart) {
                    Main.notify("SSR Proxy", `Switching to ${parsedConfig.remarks || "Unnamed"}`)
                } else {
                    Main.notify("SSR Proxy", `Connected to ${parsedConfig.remarks || "Unnamed"}`)
                }
            }
            if (http_enabled) {
                await fs.writeHTTPConfig(sock_port, http_port, this.settings.get_boolean(Share.Names.SSR_SHARE_LAN))
                let pid = await fs.readFileFromCache(Share.Names.PRIVOXY_PID_FILE_NAME)
                let alive = false;
                if (pid) {
                    pid = parseInt(pid)
                    if (Utils.isPrivoxy(pid)) {
                        alive = true;
                    }
                }
                if (!alive) {
                    let code = await Utils.startPrivoxy()
                    logger(`Privoxy start code: ${code}`)
                } else {
                    if (restart) {
                        Main.notify("SSR Proxy", `Restarting Privoxy`)
                        logger(`Restarting Privoxy`)
                        await Utils.stopPrivoxy()
                        let code = await Utils.startPrivoxy()
                        logger(`Privoxy restart with pid: ${code}`)
                    } else
                        logger(`Privoxy Daemon is alive: ${pid}`)
                }
            }
        } else {
            // Main.notify("SSR Proxy", "Tunning Off.")
            await Utils.stopSSRDaemon()
            await Utils.stopPrivoxy()
        }
    }
    buildSSRStatusSection() {
        let ssr_enabled = this.settings.get_boolean(Share.Names.SSR_ENABLE)
        this.ssrStatus = new PopupMenu.PopupMenuItem(`Shadowsocks: ${ssr_enabled ? "On" : "Off"}`);
        this.ssrStatus.actor.reactive = false
        this.menu.addMenuItem(this.ssrStatus);
        this.ssrStatusToggle = new PopupMenu.PopupMenuItem(`Turn Shadowsocks ${!ssr_enabled ? "On" : "Off"}`);
        this.ssrStatusToggle.connect('activate', () => {
            this.settings.set_boolean(Share.Names.SSR_ENABLE, !this.settings.get_boolean(Share.Names.SSR_ENABLE))
        })
        this.menu.addMenuItem(this.ssrStatusToggle);
        this.onSSRStateChanged()
    }
    initWatchers() {
        this.settings.connect(
            'changed::' + Share.Names.SSR_ENABLE, () => { this.onSSRStateChanged() })
        this.settings.connect(
            'changed::' + Share.Names.SSR_SELECTED, () => { this.onSelectedSSRLinkChanged() })
        // this.settings.connect(
        //     'changed::' + Share.Names.SSR_SUBSCRIPTION_LIST, () => {
        //         let auto_sync = this.settings.get_boolean(Share.Names.SSR_AUTO_SYNC_WHEN_CHANGED)
        //         if (auto_sync) {
        //             // use a timeout 
        //             // this.updateSubscriptionLinkAction()
        //         }
        //     })
        this.settings.connect(
            'changed::' + Share.Names.SSR_ENABLE_HTTP, () => {
                this.onPrivoxyChanged()
            }
        )
        this.settings.connect(
            'changed::' + Share.Names.SSR_SOCKS_PORT, () => {
                this.onSSRStateChanged(true)
            }
        )
        this.settings.connect(
            'changed::' + Share.Names.SSR_SHARE_LAN, () => {
                this.onSSRStateChanged(true)
            }
        )
    }
    async onPrivoxyChanged() {
        let ssr_enabled = this.settings.get_boolean(Share.Names.SSR_ENABLE)
        let privoxy_enabled = this.settings.get_boolean(Share.Names.SSR_ENABLE_HTTP)
        if (privoxy_enabled) {
            this._enable_http_item.setOrnament(PopupMenu.Ornament.CHECK)
        } else {
            this._enable_http_item.setOrnament(PopupMenu.Ornament.NONE)
        }
        let http_port = this.settings.get_int(Share.Names.SSR_HTTP_PORT)
        if (ssr_enabled) {
            await Utils.stopPrivoxy()
            if (privoxy_enabled) {
                await fs.writeHTTPConfig(this.settings.get_int(Share.Names.SSR_SOCKS_PORT), http_port)
                // TODO: check this.settings.get_string(SSR_PRIVOXY_PATH)
                let code = await Utils.startPrivoxy()
                logger(`Privoxy Restarted: ${code}`)
            }
        } else {
            logger(`SSR is closed, ignoring privoxy`)
            // ignore
        }

    }
    async buildSubListMenuItem() {
        let url_list = this.settings.get_strv(Share.Names.SSR_SUBSCRIPTION_LIST)
        if (url_list.length === 0) {
            if (!this.addSubscriptionUrl) {
                this.addSubscriptionUrl = new PopupMenu.PopupMenuItem("No Subscription");
                this.addSubscriptionUrl.actor.reactive = false;
                this.subListSection.addMenuItem(this.addSubscriptionUrl)
            }
        } else {
            if (this.addSubscriptionUrl) {
                this.addSubscriptionUrl.destroy()
                this.addSubscriptionUrl = null
            }
            for (const url of url_list) {
                let content = await fs.readSSRCache(url)
                let ssrLinkList = content.split('\n')
                for (const ssrLink of ssrLinkList) {
                    let result = Utils.parseSSRLink(ssrLink)
                    if (result && result.group) {
                        this.appendSSRLink(ssrLink, result.remarks, result.group)
                    }
                }
            }
            this.buildSSRLinkSelectedOrnament();
        }

    }
    buildSubListSection() {
        this.subListSection = new PopupMenu.PopupMenuSection()
        this.menu.addMenuItem(this.subListSection)
        this.subListMap = new Map()
        this.buildSubListMenuItem()

    }
    onSelectedSSRLinkChanged() {
        this.buildSSRLinkSelectedOrnament();
        this.onSSRStateChanged(true)
    }
    appendSSRLink(ssr_link, item_name, group_name) {
        let maxLength = this.settings.get_int(Share.Names.SSR_MAX_MENU_LENGTH)
        if (maxLength !== 0 && item_name.length > maxLength) {
            item_name = item_name.substring(0, Math.floor(maxLength / 2)) + " ... "
                + item_name.substring(item_name.length - Math.floor(maxLength / 2), item_name.length)
        }
        let targetGroup = this.subListMap.get(group_name)
        if (!targetGroup) {
            let serverListSubMenuItem = new PopupMenu.PopupSubMenuMenuItem(group_name, false);
            this.subListSection.addMenuItem(serverListSubMenuItem)
            this.subListMap.set(group_name, serverListSubMenuItem)
            targetGroup = serverListSubMenuItem;
        }
        let menuItem = new PopupMenu.PopupMenuItem(item_name);
        menuItem._ssr_link = ssr_link
        menuItem.connect('activate', (_menuItem) => {
            logger(`Selected SSR Link: ${_menuItem._ssr_link}`)
            if (_menuItem._ssr_link) {
                this.settings.set_string(Share.Names.SSR_SELECTED, _menuItem._ssr_link)
            }
        })
        targetGroup.menu.addMenuItem(menuItem)
    }
    buildSSRLinkSelectedOrnament() {
        let keys = this.subListMap.keys()
        let figured = false;
        let selectedSSR = this.settings.get_string(Share.Names.SSR_SELECTED);
        for (const groupName of keys) {
            let subMenuItem = this.subListMap.get(groupName)
            if (subMenuItem) {
                subMenuItem.setOrnament(PopupMenu.Ornament.NONE)
                let children = subMenuItem.menu._getMenuItems()
                for (const item of children) {
                    if (item._ssr_link === selectedSSR) {
                        subMenuItem.setOrnament(PopupMenu.Ornament.DOT)
                        item.setOrnament(PopupMenu.Ornament.DOT)
                        figured = true
                    } else {
                        item.setOrnament(PopupMenu.Ornament.NONE)
                    }
                }
            }
        }
        if (!figured) {
            if (selectedSSR && selectedSSR !== "") {
                Main.notify("SSR Warning", "Current SSR server info has been removed from subscription link, you might want to update your selection!")
            }
        }
    }
    buildSettingsSubMenu() {
        let http_enabled = this.settings.get_boolean(Share.Names.SSR_ENABLE_HTTP)
        this._settings_menu = new PopupMenu.PopupSubMenuMenuItem("Settings & Utils", false)
        this._settings_menu.menu.box.style_class = 'ssr-proxy-sub-menu';
        this._enable_http_item = new PopupMenu.PopupMenuItem("Enable Http");
        if (http_enabled) {
            this._enable_http_item.setOrnament(PopupMenu.Ornament.CHECK)
        }
        this._enable_http_item.connect('activate', () => {
            this.settings.set_boolean(Share.Names.SSR_ENABLE_HTTP,
                !this.settings.get_boolean(Share.Names.SSR_ENABLE_HTTP));
        })
        this._settings_menu.menu.addMenuItem(this._enable_http_item)

        let copy_proxy_command = new PopupMenu.PopupMenuItem("Copy Proxy Command");
        this._settings_menu.menu.addMenuItem(copy_proxy_command)
        copy_proxy_command.connect('activate', () => {
            let clipboard = St.Clipboard.get_default();
            clipboard.set_text(St.ClipboardType.CLIPBOARD,
                `export http_proxy=http://127.0.0.1:${this.settings.get_int(Share.Names.SSR_HTTP_PORT)}\nexport https_proxy=http://127.0.0.1:${this.settings.get_int(Share.Names.SSR_HTTP_PORT)}`);
        })
        this._settings_menu.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem())

        let all_settings_item = new PopupMenu.PopupMenuItem("Open Settings");
        all_settings_item.connect('activate', () => {
            logger("Open Settings")
            Utils.openSetting()
        })
        this._settings_menu.menu.addMenuItem(all_settings_item)
        this.menu.addMenuItem(this._settings_menu)
    }
    buildPAC() { }
    buildUpdateMenuItem() {
        this.updateAction = new PopupMenu.PopupMenuItem("Update Subscription");
        this.updateAction.connect('activate', () => {
            logger(`executing updateSubscriptionLinkAction`)
            this.updateSubscriptionLinkAction()
        })
        this.menu.addMenuItem(this.updateAction);
    }
    _init() {
        super._init(0.0, `${Me.metadata.name} Indicator`, false)
        this.sessionAsync = new Soup.SessionAsync();
        this.settings = Share.Settings
        // https://stackoverflow.com/questions/20394840/how-to-set-a-png-file-in-a-gnome-shell-extension-for-st-icon
        let icon = new St.Icon({
            gicon: Gio.icon_new_for_string(Me.path + "/icons/shadowsocks-qt5.png"),
            style_class: 'system-status-icon'
        })
        this.add_child(icon);
        this.buildSubListSection()

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem())
        this.buildSSRStatusSection()
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem())
        this.buildSettingsSubMenu();
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem())
        this.buildUpdateMenuItem()
        this.initWatchers()
    }
    /**
     * 
     * @param {string} url 
     * @returns {Promise<string>}
     */
    request(url) {
        return new Promise((resolve, reject) => {
            let request = Soup.Message.new('GET', url);
            this.sessionAsync.queue_message(request, (session, resp) => {
                if (resp.response_body.data.length > 0) {
                    logger(`Responsed: ${resp.response_body.data}`)
                    if (resp.response_body.data[0] === "<") {
                        reject(new Error("Updated Failed"))
                    } else {
                        let decoded_str = ByteArray.toString(GLib.base64_decode(resp.response_body.data))
                        resolve(decoded_str)
                    }
                }
            })
        })
    }
    async updateSubscriptionLinkAction() {
        logger(`Start updating Subscription!`)
        let urlList = this.settings.get_strv(Share.Names.SSR_SUBSCRIPTION_LIST)
        for (const url of urlList) {
            logger(`Starting fetching: ${url}`)
            try {
                let decoded_str = await this.request(url)
                await fs.writeSSRCache(url, decoded_str)
            } catch (error) {
                Main.notify("SSR Proxy", `Subscription updating failed at ${url}`)
                logger(`Updated Subscription Failed`)
                return
            }
        }
        for (const key of this.subListMap.keys()) {
            let item = this.subListMap.get(key)
            if (item) {
                item.destroy();
            }
            this.subListMap.delete(key);
        }
        await this.buildSubListMenuItem();
        Main.notify("SSR Proxy", "Subscription updating completed")

    }
    destroy() {
        super.destroy();
    }
}
if (SHELL_MINOR > 30) {
    SSRIndicator = GObject.registerClass({ GTypeName: "SSRIndicator" },
        SSRIndicator)
}

class Extension {
    constructor() {
        this.indicator = null;
    }

    enable() {
        this.indicator = new SSRIndicator();
        Main.panel.addToStatusArea(`${Me.metadata.name} Indicator`, this.indicator);
    }

    disable() {
        if (this.indicator != null) {
            this.indicator.destroy();
            this.indicator = null;
        }
    }
}

function init() {
    return new Extension();
}
