const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const ByteArray = imports.byteArray;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const fs = Me.imports.fs;
const Share = Me.imports.Share;
/**
 * 
 * @param {Array<string>} argv 
 * @param {number} flags 
 * @returns {Promise<[boolean,string,string]>}
 */
function exec(argv, flags = Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE) {
    return new Promise((resolve) => {
        let process = Gio.Subprocess.new(argv, flags);
        process.communicate_utf8_async(null, null, (source, result) => {
            let [ok, stdout, stderr] = source.communicate_utf8_finish(result)
            resolve([ok, stdout, stderr])
        })
    })
}
/**
 * 
 * @param {number} pid 
 * @returns {Promise<boolean>}
 */
async function isPythonSSR(pid) {
    // https://superuser.com/questions/632979/if-i-know-the-pid-number-of-a-process-how-can-i-get-its-name
    const [ok, stdout, stdderr] = await exec(["ps", "-p", pid.toString(), "-o", "command="]);
    if (stdout) {
        let commands = stdout.split(" ");
        if (commands.length > 2) {
            if (commands[0].indexOf("python") !== -1 &&
                commands[1].indexOf("local.py") !== -1 &&
                commands[1].indexOf("shadowsocks") !== -1) {
                return true;
            }
        }
    }
    return false;
}
async function isPrivoxy(pid) {
    const [ok, stdout, stdderr] = await exec(["ps", "-p", pid.toString(), "-o", "command="]);
    if (stdout) {
        let commands = stdout.split(" ");
        if (commands.length > 2) {
            if (commands[0].indexOf("privoxy") !== -1) {
                return true;
            }
        }
    }
    return false;
}
/**
 * 
 * @param {number} pid 
 */
async function kill(pid) {
    await exec(["kill", "-15", pid.toString()]);
}

function valid_http_link(link) {

}

/**
 * 
 * @param {string} str 
 * @returns {string}
 */
function decode(str) {
    let completed = str + Array(5 - str.length % 4).join('=')
    completed = completed.replace(/\-/g, '+').replace(/\_/g, '/');
    return ByteArray.toString(GLib.base64_decode(completed))
}
/**
 * 
 * @param {string} link 
 * @returns {{}|null}
 */
function parseSSRLink(link) {
    try {
        const body = link.substring(6)
        const decoded = decode(body)
        const _split = decoded.split('/?')
        const required = _split[0]
        const others = _split[1]
        const requiredSplit = required.split(':')
        if (requiredSplit.length !== 6) {
            return null
        }
        let ssrPara = {
            server: requiredSplit[0],
            server_port: parseInt(requiredSplit[1]),
            protocol: requiredSplit[2],
            method: requiredSplit[3],
            obfs: requiredSplit[4],
            password: decode(requiredSplit[5])
        }

        others && others.split('&').forEach(item => {
            const _params = item.split('=')
            let name = _params[0]
            if (name === "obfsparam") {
                name = "obfs_param"
            }
            if (name === "protoparam") {
                name = "protocol_param"
            }
            ssrPara[name] = decode(_params[1])
        })
        return ssrPara
    } catch (e) {
        logError(e)
        return null
    }
}

function startSSRDaemon() {
    return execSSRDaemonWithOperation(1)
}
function stopSSRDaemon() {
    return execSSRDaemonWithOperation(-1)
}
function restartSSRDaemon() {
    return execSSRDaemonWithOperation(0)
}

function execSSRDaemonWithOperation(operation) {
    let argv = []
    argv.push(Share.Settings.get_string(Share.Names.SSR_SCRIPT_PATH))
    argv.push("-c")
    argv.push(Share.Path.SSR_CONFIG_FILE_PATH)
    argv.push("--pid-file")
    argv.push(Share.Path.SSR_PID_FILE_PATH)
    argv.push("--log-file")
    argv.push(Share.Path.LOG_FILE_PATH)
    argv.push("-d")
    let operation_str = "start"
    if (operation === -1) {
        operation_str = "stop"
    } else if (operation === 0) {
        operation_str = "restart"
    }
    argv.push(operation_str)
    return new Promise((resolve, reject) => {
        let proc = Gio.Subprocess.new(argv, Gio.SubprocessFlags.NONE);
        proc.wait_async(null, (source, result) => {
            let exit_code = source.get_exit_status()
            resolve(exit_code)
        })
    })
}

function startPrivoxy() {
    let argv = []
    argv.push(Share.Settings.get_string(Share.Names.SSR_PRIVOXY_PATH))
    argv.push("--pidfile")
    argv.push(Share.Path.PRIVOXY_PID_FILE_PATH)
    argv.push(Share.Path.PRIVOXY_CONFIG_FILE_PATH)
    return new Promise((resolve, reject) => {
        let proc = Gio.Subprocess.new(argv, Gio.SubprocessFlags.NONE);
        proc.wait_async(null, (source, result) => {
            let exit_code = source.get_exit_status()
            resolve(exit_code)
        })
    })
}
async function stopPrivoxy() {
    const content = await fs.readFileFromCache(Share.Names.PRIVOXY_PID_FILE_NAME);
    if (content) {
        let pid = parseInt(content);
        return kill(pid);
    }
}
function openSetting() {
    Gio.Subprocess.new(["gnome-shell-extension-prefs", "ssr-proxy@xvanturing.gitlab.com"], Gio.SubprocessFlags.NONE);
}