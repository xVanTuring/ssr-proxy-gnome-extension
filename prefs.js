'use strict';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const GObject = imports.gi.GObject;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Share = Me.imports.Share;
function init() {
}
const SSRSubscriptionListModel = GObject.registerClass(
    class extends Gtk.ListStore {
        _init(params) {
            super._init(params)
            this.set_column_types([GObject.TYPE_STRING])
            this.Columns = {
                LABEL: 0
            }
            this._settings = ExtensionUtils.getSettings();
            this._reloadFromSettings()

            this.connect('row-changed', this._onRowChanged.bind(this));
            this.connect('row-inserted', this._onRowInserted.bind(this));
            this.connect('row-deleted', this._onRowDeleted.bind(this));
        }
        _reloadFromSettings() {
            if (this._preventChanges) {
                return
            }
            this._preventChanges = true
            let subscriotionList = this._settings.get_strv(Share.Names.SSR_SUBSCRIPTION_LIST)
            for (const url of subscriotionList) {
                let iter = this.append();
                this.set(iter, [this.Columns.LABEL], [url])
            }
            this._preventChanges = false
        }
        _onRowChanged(self, path, iter) {
            if (this._preventChanges)
                return;
            this._preventChanges = true;

            let index = path.get_indices()[0]
            let urls = this._settings.get_strv(Share.Names.SSR_SUBSCRIPTION_LIST);
            if (index >= urls.length) {
                for (let i = urls.length; i <= index; i++)
                    urls[i] = '';
            }
            urls[index] = this.get_value(iter, this.Columns.LABEL);
            this._settings.set_strv(Share.Names.SSR_SUBSCRIPTION_LIST, urls);
            this._preventChanges = false;
        }
        _onRowDeleted(self, path) {
            if (this._preventChanges)
                return;
            this._preventChanges = true;
            let index = path.get_indices()[0];
            let urls = this._settings.get_strv(Share.Names.SSR_SUBSCRIPTION_LIST);
            if (index >= urls.length) {
                return;
            }
            urls.splice(index, 1)
            this._settings.set_strv(Share.Names.SSR_SUBSCRIPTION_LIST, urls);
            this._preventChanges = false;
        }
        _onRowInserted(self, path, iter) {
            if (this._preventChanges)
                return;
            this._preventChanges = true;
            let index = path.get_indices()[0];
            let urls = this._settings.get_strv(Share.Names.SSR_SUBSCRIPTION_LIST);
            let url = this.get_value(iter, this.Columns.LABEL) || '';
            urls.splice(index, 0, url);
            this._settings.set_strv(Share.Names.SSR_SUBSCRIPTION_LIST, urls);
            this._preventChanges = false;
        }
    });
class SSRWidget {
    constructor() {
        this.settings = ExtensionUtils.getSettings();
        this.prefsWidget = new Gtk.Grid({
        });
        this.prefsWidget.set_orientation(Gtk.Orientation.VERTICAL)
        this.buildBooleanSwitch("Enable SSR", Share.Names.SSR_ENABLE)
        this.buildBooleanSwitch("Allow Connection from local LAN", Share.Names.SSR_SHARE_LAN, "ssr restart required")
        this.buildIntegerInput("Local Port", Share.Names.SSR_SOCKS_PORT, 1, 60000, "Local Socks Port")
        this.buildIntegerInput("Cache Live in hour", Share.Names.SSR_CACHE_LIVE, -1, 720, "-1: no cache 0: no update")
        this.buildIntegerInput("Menu Item length", Share.Names.SSR_MAX_MENU_LENGTH, 0, 200, "0: display all")
        // this.buildBooleanSwitch("Use Shadowsocks", Share.Names.SSR_USE_SS)
        // this.buildTextInput("SS Path", Share.Names.SSR_SS_PATH, "Path to ss-local")
        this.buildTextInput("SSR Path", Share.Names.SSR_SCRIPT_PATH, "Path to local.py")
        this.buildBooleanSwitch("Auto Sync when Subscription Changed", Share.Names.SSR_AUTO_SYNC_WHEN_CHANGED)
        this.buildSubScriptionList()
        this.buildBooleanSwitch("Enable Http Proxy", Share.Names.SSR_ENABLE_HTTP)
        this.buildIntegerInput("Http Port", Share.Names.SSR_HTTP_PORT, 1, 60000, "Local Socks Port")
        this.buildTextInput("Privoxy Path", Share.Names.SSR_PRIVOXY_PATH, "Path to Privoxy executable")
    }
    buildSubScriptionList() {
        let subscription_list_box = new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            margin: 8
        })

        let ssrSubListLabel = new Gtk.Label({
            label: `<b>Subscription URLs</b>`,
            xalign: 0,
            use_markup: true,
            margin_bottom: 6
        });
        subscription_list_box.add(ssrSubListLabel)

        let scrolled = new Gtk.ScrolledWindow({ shadow_type: Gtk.ShadowType.IN })
        scrolled.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC);
        subscription_list_box.add(scrolled)

        this._store = new SSRSubscriptionListModel();
        this._treeView = new Gtk.TreeView({
            model: this._store,
            hexpand: true,
            vexpand: true,
            headers_visible: false,
            reorderable: true,
            hexpand: true,
            vexpand: true,
        })
        this._treeView.get_selection().set_mode(Gtk.SelectionMode.SINGLE);

        let column = new Gtk.TreeViewColumn({
            title: "Url"
        });
        let renderer = new Gtk.CellRendererText({ editable: true });
        renderer.connect('edited', this.onCellEdited.bind(this))
        column.pack_start(renderer, true);
        column.add_attribute(renderer, "text", this._store.Columns.LABEL);
        this._treeView.append_column(column);
        scrolled.add(this._treeView)


        let toolbar = new Gtk.Toolbar({
            icon_size: Gtk.IconSize.SMALL_TOOLBAR
        });
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR);
        subscription_list_box.add(toolbar);

        let newButton = new Gtk.ToolButton({
            icon_name: 'list-add-symbolic'
        });
        newButton.connect('clicked', this.onNewClicked.bind(this));
        toolbar.add(newButton);

        let delButton = new Gtk.ToolButton({ icon_name: 'list-remove-symbolic' });
        delButton.connect('clicked', this.onDeletedClicked.bind(this));
        toolbar.add(delButton);

        let selection = this._treeView.get_selection()
        selection.connect('changed', () => {
            delButton.sensitive = selection.count_selected_rows() > 0
        })
        delButton.sensitive = selection.count_selected_rows() > 0

        this.prefsWidget.add(subscription_list_box)
    }
    buildBooleanSwitch(label_str, key, tool_tip = "") {
        let box = new Gtk.Box({
            orientation: Gtk.Orientation.HORIZONTAL,
            margin: 8
        })
        let label = new Gtk.Label({
            label: label_str,
            xalign: 0
        });
        if (tool_tip) {
            label.set_tooltip_text(tool_tip)
        }
        let toggle = new Gtk.Switch({
            active: this.settings.get_boolean(key),
        });
        box.pack_start(label, true, true, 0)
        box.add(toggle)

        this.settings.bind(
            key,
            toggle,
            'active',
            Gio.SettingsBindFlags.DEFAULT
        );
        this.prefsWidget.add(box)
    }
    buildIntegerInput(label_str, key, min, max, tool_tip = "") {
        let box = new Gtk.Box({
            orientation: Gtk.Orientation.HORIZONTAL,
            margin: 8
        })
        let label = new Gtk.Label({
            label: label_str,
            xalign: 0
        });
        if (tool_tip) {
            label.set_tooltip_text(tool_tip)
        }
        let integer_input = new Gtk.SpinButton({
            adjustment: new Gtk.Adjustment({
                lower: min,
                upper: max,
                step_increment: 1
            })
        });
        box.pack_start(label, true, true, 0)
        box.add(integer_input)
        this.settings.bind(key, integer_input, 'value', Gio.SettingsBindFlags.DEFAULT)
        this.prefsWidget.add(box)
    }
    buildTextInput(label_str, key, hint, nchar = 35, tool_tip = "") {
        let box = new Gtk.Box({
            orientation: Gtk.Orientation.HORIZONTAL,
            margin: 8
        })
        let label = new Gtk.Label({
            label: label_str,
            xalign: 0
        });
        if (tool_tip) {
            label.set_tooltip_text(tool_tip)
        }
        let text_input = new Gtk.Entry()
        text_input.set_width_chars(nchar)
        text_input.set_placeholder_text(hint)
        text_input.set_text(this.settings.get_string(key))
        text_input.connect("changed", (source) => {
            this.settings.set_string(key, source.get_text())
        })
        text_input.connect("activate", (source) => {
            this.settings.set_string(key, source.get_text())
        })
        box.pack_start(label, true, true, 0)
        box.add(text_input)
        this.prefsWidget.add(box)
    }
    onCellEdited(renderer, path, newText) {
        let [ok, iter] = this._store.get_iter_from_string(path)
        if (ok) {
            this._store.set(iter, [this._store.Columns.LABEL], [newText]);
        }
    }
    onNewClicked() {
        let iter = this._store.append();
        this._store.set(iter, [this._store.Columns.LABEL], ["<Empty Subscription>"])
    }
    onDeletedClicked() {
        let [selected, model_, iter] = this._treeView.get_selection().get_selected();
        if (selected) {
            this._store.remove(iter)
        }
    }
}
function buildPrefsWidget() {

    let widget = new SSRWidget();
    widget.prefsWidget.show_all()
    return widget.prefsWidget;
}
