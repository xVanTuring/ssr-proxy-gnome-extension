export function startSSRDaemon(ssr_path: string, ): Promise<number>;
export function stopSSRDaemon(ssr_path: string, ): Promise<number>;
export function restartSSRDaemon(ssr_path: string): Promise<number>;

export function startPrivoxy(privoxy_path: string): Promise<number>;

export function stopPrivoxy(): Promise<[boolean, string, string]>;
export function isPythonSSR(pid: number | string): Promise<boolean>;
export function isPrivoxy(pid: number | string): Promise<boolean>;
export function openSetting(): void;
export interface SSRConfig {
    server: string,
    server_port: string,
    protocol: string,
    method: string,
    obfs: string,
    password: string,
    obfs_param?: string,
    protocol_param?: string
}
export function parseSSRLink(link: string): SSRConfig | null;