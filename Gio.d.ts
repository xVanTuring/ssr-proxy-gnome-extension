import * as GLib from "./GLib";

declare class Icon
{
    static new_for_string(str: string): Icon;
    equal(icon2: Icon): boolean;
    serialize(): import("./GLib").Variant;
    to_string(): string | null;
}
declare function icon_new_for_string(str: string): Icon;

declare enum SubprocessFlags
{
    NONE,
    STDIN_PIPE,
    STDIN_INHERIT,
    STDOUT_PIPE,
    STDOUT_SILENCE,
    STDERR_PIPE,
    STDERR_SILENCE,
    STDERR_MERGE,
    INHERIT_FDS
}
declare class Cancellable
{
}
interface Initable
{
    init(cancellable: Cancellable): void;
    // static newv(): Initable;
}
//TODO: GObject
declare interface AsyncResult
{
}
declare class Task implements AsyncResult
{
    set_task_data(): void;
    get_task_data(): any;
}
type AsyncReadyCallback<T> = (source_object: T, result: AsyncResult) => void;
declare class Subprocess
{
    constructor(cfg: { argv?: Array<string>, flags?: SubprocessFlags });
    static new(argv: Array<string>, flags: SubprocessFlags): Subprocess;
    argv: Array<string>;
    flags: SubprocessFlags;
    init(cancellable: any): void;
    communicate(stdin_buf: Uint8Array, cancellable: Cancellable): [boolean, Uint8Array, Uint8Array];
    communicate_async(stdin_buf: Uint8Array, cancellable: Cancellable, callback: AsyncReadyCallback<Subprocess>): void;
    communicate_finish(result: AsyncResult): [boolean, Uint8Array, Uint8Array];
    communicate_utf8(stdin_buf: string, cancellable: Cancellable): [boolean, string, string];
    communicate_utf8_async(stdin_buf: string, cancellable: Cancellable | null, callback: AsyncReadyCallback<Subprocess>): void;
    communicate_utf8_finish(result: AsyncResult): [boolean, string, string];
    force_exit(): void;
    get_exit_status(): number;
    get_identifier(): string;
    get_if_exited(): boolean;
    get_if_signaled(): boolean;
    get_status(): number;
    get_stderr_pipe(): InputStream;
    get_stdin_pipe(): InputStream;
    get_stdout_pipe(): InputStream;
    get_successful(): boolean;
    get_term_sig(): boolean;
    send_signal(singal: number): void;
    wait(cancellable: Cancellable): boolean;
    wait_async(cancellable: Cancellable, callback: AsyncReadyCallback<Subprocess>): void;
    wait_check(cancellable: Cancellable): boolean;
    wait_check_async(cancellable: Cancellable, callback: AsyncReadyCallback<Subprocess>): void;
    wait_check_finish(result: AsyncResult): boolean;
    wait_finish(result: AsyncResult): boolean;
}

/* #region Stream */
declare abstract class InputStream
{
    clear_pending(): void;
    close(cancellable: Cancellable): boolean;
    close_async(io_priority: number, cancellable: Cancellable, callback: AsyncReadyCallback<InputStream>): void;
    close_finish(result: AsyncResult): boolean;
    has_pending(): boolean;
    is_closed(): boolean;
    read(cancellable: Cancellable): number;
    read_all(cancellable: Cancellable): [boolean, number];
    read_all_async(io_priority: number, cancellable: Cancellable, callback: AsyncReadyCallback<InputStream>): void;
    read_all_finish(result: AsyncResult): [boolean, number];
    read_async(io_priority: number, cancellable: Cancellable, callback: AsyncReadyCallback<InputStream>): void;
    read_bytes(count: number, cancellable: Cancellable): GLib.Bytes;
    read_bytes_async(count: number, io_priority: number, cancellable: Cancellable, callback: AsyncReadyCallback<InputStream>): void;
    read_bytes_finish(result: AsyncResult): GLib.Bytes;
    read_finish(result: AsyncResult): number;
    set_pending(): boolean;
    skip(count: number, cancellable: Cancellable): number;
    skip_async(count: number, io_priority: number, cancellable: Cancellable, callback: AsyncReadyCallback<InputStream>): void;
    skip_finish(result: AsyncResult): number;
}
declare class Source
{

    add_child_source(): void;
    add_poll(): void;
    attach(): void;
    destroy(): void;
    get_can_recurse(): void;
    get_context(): void;
    get_current_time(): void;
    get_id(): void;
    get_name(): void;
    get_priority(): void;
    get_ready_time(): void;
    get_time(): void;
    is_destroyed(): void;
    modify_unix_fd(): void;
    query_unix_fd(): void;
    ref(): void;
    remove_child_source(): void;
    remove_poll(): void;
    remove_unix_fd(): void;
    set_callback(func: GLib.SourceFunc): void;
    set_callback_indirect(): void;
    set_can_recurse(): void;
    set_name(): void;
    set_priority(): void;
    set_ready_time(): void;
    unref(): void;

    static remove(): void;
    static remove_by_user_data(): void;
    static set_name_by_id(): void;

}
declare interface PollableInputStream extends InputStream
{

    can_poll(): boolean;
    create_source(cancellable: Cancellable | null): Source;
    is_readable(): boolean;
    read_nonblocking(): void;
}

declare class UnixInputStream extends InputStream implements PollableInputStream
{
    constructor(cfg: { fd: number, close_fd?: boolean });
    static new(fd: number, close_fs: boolean): UnixInputStream;
    get_close_fd(): boolean;
    get_fd(): boolean;
    set_close_fd(close_fd: boolean): void;
    fd: number;
    close_fd: boolean;
    can_poll(): boolean;
    create_source(cancellable: Cancellable | null): Source;
    is_readable(): boolean;
    read_nonblocking(): void;
}
/* #endregion */
declare class FileIOStream
{

}
declare class FileOutputStream
{
}
declare class File
{
    static new_for_path(path: string): File;
    static new_for_commandline_arg(arg: string): File;
    static new_for_commandline_arg_and_cwd(arg: string, cwd: string): File;
    static new_for_path(path: string): File;
    static new_for_uri(uri: string): File;
    static new_tmp(tmpl: string): [File, FileIOStream];
    static parse_name(parse_name: string): File;

    load_contents_async(cancellable: Cancellable, callback: AsyncReadyCallback<File>): void;
    load_contents(cancellable: Cancellable): [boolean, Uint8Array, string];
    replace_async(etag: string, make_backup: boolean, flags: FileCreateFlags,
        io_priority: number, cancellable: Cancellable, callback: AsyncReadyCallback<File>): void;
    replace(etag: string, make_backup: boolean, flags: FileCreateFlags, cancellable: Cancellable): FileOutputStream;
    replace_finish(res: AsyncResult): FileOutputStream;

}
declare enum FileCreateFlags
{
    NONE,
    PRIVATE,
    REPLACE_DESTINATION
}
// TODO: 
declare interface Settings
{
    get_string(key: string): string;
    get_boolean(key: string): boolean;
    get_int(key: string): number;

    set_string(key: string, value: string): void;
    set_boolean(key: string, value: boolean): void;
    set_int(key: string, value: number): void;
}