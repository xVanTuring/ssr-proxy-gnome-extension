/* #region Utils */

export function usleep(microseconds: number): void;
/**
 * A wrapper for the POSIX access() function. This function is used to test a pathname for one or several of read, write or execute permissions, or just existence.
 * 
 * On Windows, the file protection mechanism is not at all POSIX-like, and the underlying function in the C library only checks the FAT-style READONLY attribute, and does not look at the ACL of a file at all. This function is this in practise almost useless on Windows. Software that needs to handle file permissions on Windows more exactly should use the Win32 API.
 * 
 * See your C library manual for more details about access().
 * 
 * @param filename a pathname in the GLib file name encoding (UTF-8 on Windows)
 * @param mode as in access()
 * @returns zero if the pathname refers to an existing file system object that has all the tested permissions, or -1 otherwise or on error.
 */
export function access(filename: string, mode: number): number;
/**
 * Determines the numeric value of a character as a decimal digit. Differs from {@link g_unichar_digit_value()} because it takes a char, so there's no worry about sign extension if characters are signed.
 * @param c an ASCII character
 * @returns If c is a decimal digit (according to g_ascii_isdigit()), its numeric value. Otherwise, -1.
 */
export function ascii_digit_value(c: number): number;

/**
 * Converts a float to a string, using the ‘.’ as decimal point.
 * 
 * This function does NOT use GLib
 * @param unused not used param 
 * @param len 
 * @returns The pointer to the buffer with the converted string.
 */
export function ascii_dtostr(unused: any, len: number, number: number): string;
/**
 * Use `Number.toExponential()` and string interpolation instead
 */
export function ascii_formatd(): void;

/**
 * Compare two strings, ignoring the case of ASCII characters.
 * 
 * Unlike the BSD strcasecmp() function, this only recognizes standard ASCII letters and ignores the locale, treating all non-ASCII bytes as if they are not letters.
 * 
 * This function should be used only on strings that are known to be in encodings where the bytes corresponding to ASCII letters always represent themselves. This includes UTF-8 and the ISO-8859-* charsets, but not for instance double-byte encodings like the Windows Codepage 932, where the trailing bytes of double-byte characters include all ASCII letters. If you compare two CP932 strings using this function, you will get false matches.
 * @param s1  string to compare with `s2`
 * @param s2  string to compare with `s1`
 * @returns 0 if the strings match, a negative value if `s1` < `s2`, or a positive value if `s1` > `s2`.
 */
export function ascii_strcasecmp(s1: string, s2: string): number;

/**
 * Converts all upper case ASCII letters to lower case ASCII letters.
 * @param str a string
 * @param len length of str
 * @returns a newly-allocated string, with all the upper case characters in str converted to lower case, with semantics that exactly match `GLib.ascii_tolower()`. (Note that this is unlike the old `GLib.strdown()`, which modified the string in place.)
 */
export function ascii_strdown(str: string, len: number): string;

export function ascii_string_to_signed(str: string, base: number, min: number, max: number): boolean;
export function ascii_string_to_unsigned(str: string, base: number, min: number, max: number): boolean;
export function ascii_strncasecmp(s1: string, s2: string, n: number): number;
export function ascii_strtod(nptr: string, ): number;

export function base64_decode(text: string): Uint8Array
export function base64_encode(text: string | Uint8Array): string;
export function basename(filename: string): string;
export function build_filenamev(args: Array<string>): string;
export function build_pathv(separator: string, args: Array<string>): string;
/**
 * 
 * @param required_major 
 * @param required_minor 
 * @param required_micro 
 * @returns `null` if the GLib library is compatible with the given version, or a string describing the version mismatch. The returned string is owned by GLib and must not be modified or freed.
 */
export function check_version(required_major: number, required_minor: number, required_micro: number): string;
export function close(fd: number): boolean;
/* #endregion */
export class Bytes
{
    constructor(data: string);
    toArray(): Uint8Array;
}
export class MainContext
{
}
export class MainLoop
{
    constructor(context: MainContext, is_running: boolean);
    static new(context: MainContext, is_running: boolean): MainLoop;
    get_context(): MainContext;
    is_running(): boolean;
    quit(): void;
    /**
     * Increases the reference count on a `GLib.MainLoop` object by one.
     */
    ref(): MainLoop;
    run(): void;
    unref(): void;
}
/* #region SubProcess */
export enum SpawnFlags
{
    DEFAULT,
    LEAVE_DESCRIPTORS_OPEN,
    DO_NOT_REAP_CHILD,
    SEARCH_PATH,
    STDOUT_TO_DEV_NULL,
    STDERR_TO_DEV_NULL,
    CHILD_INHERITS_STDIN,
    FILE_AND_ARGV_ZERO,
    SEARCH_PATH_FROM_ENVP,
    CLOEXEC_PIPES
}
type SpawnChildSetupFunc = () => void;
/**
 * @param workding_directory child's current working directory, or null to inherit parent's
 * @param argv child's argument vecto
 * @param envp child's environment, or null to inherit parent's
 * @param flags flags from GLib.SpawnFlags
 * @param child_setup function to run in the child just before exec()
 * @returns ok: true on success, false if error is set, child_pid: return location for child process reference, or null
 */
export function spawn_async(workding_directory: string | null, argv: Array<string>,
    envp: Array<string>, flags: SpawnFlags, child_setup: SpawnChildSetupFunc): [boolean, number | null];
/**
 * 
 * @param workding_directory child's current working directory, or null to inherit parent's
 * @param argv child's argument vecto
 * @param envp child's environment, or null to inherit parent's
 * @param flags flags from GLib.SpawnFlags
 * @param child_setup function to run in the child just before exec()
 * @returns [ok, child_pid, standard_input, standard_output, standard_error]
 */
export function spawn_async_with_pipes(workding_directory: string | null, argv: Array<string>,
    envp: Array<string>, flags: SpawnFlags, child_setup: SpawnChildSetupFunc):
    [boolean, number | null, number | null, number | null, number | null];
export function spawn_check_exit_status(exit_status: number): boolean;
export function spawn_close_pid(pid: number): void;
export function spawn_command_line_async(command_line: string): boolean;
/**
 * 
 * @param command_line a command line
 * @returns ok
 * @returns standard_output
 * @returns standard_error
 * @returns exit_status
 */
export function spawn_command_line_sync(command_line: string): [boolean, Uint8Array, Uint8Array, number];
// export function spawn_error_quark();
// export function spawn_exit_error_quark();
export function spawn_sync(workding_directory: string | null, argv: Array<string>,
    envp: Array<string>, flags: SpawnFlags, child_setup: SpawnChildSetupFunc):
    [boolean, Uint8Array, Uint8Array, number];
type ChildWatchFunc = (pid: number, status: number) => void;

type PRIORITY = number;
export const PRIORITY_DEFAULT: PRIORITY;
export const PRIORITY_HIGH: PRIORITY;
export const PRIORITY_LOW: PRIORITY;
/**
 * 
 * @param priority the priority of the idle source. between 200 to 300
 * @param pid process to watch. 
 * @param func function to call
 * @returns the ID (greater than 0) of the event source.
 */
export function child_watch_add(priority: PRIORITY, pid: number, func: ChildWatchFunc): number;
/* #endregion */
/* #region FS */
export enum FileTest
{
    IS_REGULAR,
    IS_SYMLINK,
    IS_DIR,
    IS_EXECUTABLE,
    EXISTS
}
export function file_test(filename: string, test: FileTest): boolean;

export function mkdir_with_parents(pathname: string, mode: number): number;
/**
 * A wrapper for the POSIX chdir() function. The function changes the current directory of the process to path.
 * @param path a pathname in the GLib file name encoding (UTF-8 on Windows)
 * @returns 0 on success, -1 if an error occurred.
 */
export function chdir(path: string): number;

/* #endregion */

/* #region get */
export function get_application_name(): string;
// TODO: not clear
export function get_charset(charset: string): boolean;
export function get_codeset(): string;
export function get_current_dir(): string;
export class TimeVal
{
    tv_sec: number
    tv_usec: number
    constructor(cfg?: { tv_sec: number, tv_usec: number });
}
export function get_current_time(result: TimeVal): void;
export function get_environ(): Array<string>;
export function get_filename_charsets(charsets: string): boolean;
export function get_home_dir(): string;
export function get_host_name(): string;
export function get_language_names(): Array<string>;
export function get_locale_variants(): Array<string>;
export function get_monotonic_time(): number;
export function get_num_processors(): number;
export function get_prgname(): string;
export function get_real_name(): string;
export function get_real_time(): number;
export function get_system_config_dirs(): Array<string>;
export function get_system_data_dirs(): Array<string>;
export function get_tmp_dir(): string;
export function get_user_cache_dir(): string;
export function get_user_config_dir(): string;
export function get_user_data_dir(): string;
export function get_user_name(): string;
export function get_user_runtime_dir(): string;
export enum UserDirectory
{
    DIRECTORY_DESKTOP,
    DIRECTORY_DOCUMENTS,
    DIRECTORY_DOWNLOAD,
    DIRECTORY_MUSIC,
    DIRECTORY_PICTURES,
    DIRECTORY_PUBLIC_SHARE,
    DIRECTORY_TEMPLATES,
    DIRECTORY_VIDEOS,
    N_DIRECTORIES
}
export function get_user_special_dir(directory: UserDirectory): string;
export function getenv(variable: string): string;
/* #endregion */

export type Variant = any;
type SourceFuncReturn = boolean;
export const SOURCE_CONTINUE: SourceFuncReturn;
export const SOURCE_REMOVE: SourceFuncReturn;

/**
 * @returns false if the source should be removed. `GLib.SOURCE_CONTINUE` and `GLib.SOURCE_REMOVE` are more memorable names for the return value.
 */
type SourceFunc = () => SourceFuncReturn;
/* #region time */
export function timeout_add_seconds(priority: PRIORITY, interval: number, func: SourceFunc): number;
/**
 * 
 * @param priority the priority of the timeout source.
 * @param interval the time between calls to the function, in milliseconds (1/1000ths of a second)
 * @param func function to call
 * @returns the ID (greater than 0) of the event source.
 */
function timeout_add(priority: PRIORITY, interval: number, func: SourceFunc): number;

function idle_add(priority: number, func: SourceFunc): number;
/* #endregion */

