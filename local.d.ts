declare module "local" {
    export interface imports
    {
        fs: typeof import("./fs"),
        Utils: typeof import("./Utils"),
        Share: typeof import("./Share"),
    }
}