declare class Actor
{
    add_actor(): void;
    add_style_class_name(style_name: string): any;
    add_child(): void;
    connect(name: string, callback: Function): void;
    disconnect(callback: Function): void;
}
declare class ButtonType
{
    destroy(): void;
    actor: Actor;
    _init(menuAlignment: number, nameText: string, dontCreateMenu: boolean): void;
    menu?: {
        addAction: (name: string, action: () => void, some: any) => void;
        addMenuItem: () => void;
    }
}
type GIcon = any;
interface importsType
{
    ui: {
        main: {
            panel: {
                addToStatusArea: (name: string, view: any) => void;
                statusArea: {
                    aggregateMenu: {
                        _volume: {
                            reactive: boolean,
                            _onScrollEvent: Function
                        }
                    }
                }
            }
        },
        panelMenu: {
            Button: new () => ButtonType
        },
        popupMenu: {
            PopupMenuSection: new () => any;
            PopupMenuItem: new () => {
                label: {
                    set_text: (label: string) => void;
                }
            };
            PopupSubMenuMenuItem: new (label: string, wantIcon: boolean) => any;
        }
    },
    gi: {
        St: {
            Icon: new (config: { gicon: GIcon, style_class: string }) => {
                style_class: string,
                gicon: GIcon
            };
            BoxLayout: new () => any;
            ScrollView: new () => any;
        },
        Gio: typeof import("./Gio"),
        GLib: typeof import("./GLib"),
        GObject: {
            registerClass<T>(config: { GTypeName: string }, type: { new(): T }): T;
        }
    },
    misc: {
        extensionUtils: {
            getCurrentExtension: () => Extension;
            getSettings: () => import("./Gio").Settings;
        },
        config: {
            PACKAGE_VERSION: string
        };
    },
    byteArray: typeof import("./ByteArray")
}
declare var imports: importsType;

declare function log(message: any, ): void;
declare function logError(message: any, ): void;
declare interface Extension
{
    uuid: string,
    metadata: {
        name: string,

    },
    imports: import("local").imports
}
