import { Settings } from './Gio'
interface iNames
{
    SSR_SUBSCRIPTION_LIST: string,
    SSR_AUTO_SYNC_WHEN_CHANGED: string,
    SSR_ENABLE: string,
    SSR_ENABLE_HTTP: string,
    SSR_SOCKS_PORT: string,
    SSR_HTTP_PORT: string,
    SSR_SHARE_LAN: string,
    SSR_CACHE_LIVE: string,
    SSR_MAX_MENU_LENGTH: string,
    SSR_SCRIPT_PATH: string,
    SSR_PRIVOXY_PATH: string,
    SSR_USE_SS: string,
    SSR_SS_PATH: string,
    SSR_SELECTED: string,


    SSR_PID_FILE_NAME: string,
    PRIVOXY_PID_FILE_NAME: string,
    LOG_FILE_NAME: string,
    SSR_CONFIG_FILE_NAME: string,
    PRIVOXY_CONFIG_FILE_NAME: string,
}
interface iPath
{
    CACHE_DIR: string,
    SSR_PID_FILE_PATH: string,
    PRIVOXY_PID_FILE_PATH: string,
    LOG_FILE_PATH: string,
    PRIVOXY_CONFIG_FILE_PATH: string,
    SSR_CONFIG_FILE_PATH: string
}
export const Names: iNames;
export const Path: iPath;
export const Settings: Settings;