const GLib = imports.gi.GLib;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const CACHE_DIR = GLib.get_user_cache_dir() + '/' + Me.uuid;

var Names = {
    SSR_SUBSCRIPTION_LIST: "subscription-list",
    SSR_AUTO_SYNC_WHEN_CHANGED: "auto-sync-when-changed",
    SSR_ENABLE: "ssr-enabled",
    SSR_ENABLE_HTTP: "ssr-enable-http",
    SSR_SOCKS_PORT: "ssr-port",
    SSR_HTTP_PORT: "ssr-http-port",
    SSR_SHARE_LAN: "ssr-share-lan",
    SSR_CACHE_LIVE: "cache-live",
    SSR_MAX_MENU_LENGTH: "max-menu-length",
    SSR_SCRIPT_PATH: "ssr-script-path",
    SSR_PRIVOXY_PATH: "ssr-privoxy",
    SSR_USE_SS: "ssr-use-ss",
    SSR_SS_PATH: "ss-path",
    SSR_SELECTED: "selected-ssr",


    SSR_PID_FILE_NAME: "ssr.pid",
    PRIVOXY_PID_FILE_NAME: "privoxy.pid",

    LOG_FILE_NAME: "log_file",
    SSR_CONFIG_FILE_NAME: "config.json",
    PRIVOXY_CONFIG_FILE_NAME: "privoxy.cfg",
}
var Path = {
    CACHE_DIR: CACHE_DIR,
    SSR_PID_FILE_PATH: CACHE_DIR + '/' + Names.SSR_PID_FILE_NAME,
    PRIVOXY_PID_FILE_PATH: CACHE_DIR + '/' + Names.PRIVOXY_PID_FILE_NAME,
    LOG_FILE_PATH: CACHE_DIR + '/' + Names.LOG_FILE_NAME,
    PRIVOXY_CONFIG_FILE_PATH: CACHE_DIR + '/' + Names.PRIVOXY_CONFIG_FILE_NAME,
    SSR_CONFIG_FILE_PATH: CACHE_DIR + '/' + Names.SSR_CONFIG_FILE_NAME
}
var Settings = ExtensionUtils.getSettings()

