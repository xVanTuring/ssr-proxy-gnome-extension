export function writeSSRCache(url: string, content: string): Promise<undefined>;
export function readSSRCache(url: string): Promise<string | null>;
export function writeHTTPConfig(socket_port: number, http_port: number): Promise<undefined>;

export function writeFileToCache(file_name: string, content: string): Promise<undefined>;
export function readFileFromCache(file_name: string): Promise<string | undefined>