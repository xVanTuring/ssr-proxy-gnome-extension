const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const ByteArray = imports.byteArray
const FileTest = GLib.FileTest;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const Share = Me.imports.Share;
const SSRPROXY = "SSR-PROXY: "
/**
 * 
 * @param {string} url 
 * @param {string} content 
 */
function writeSSRCache(url, content) {
    let file_name = encodeURIComponent(url)
    return writeFileToCache(file_name, content)
}
/**
 * 
 * @param {string} url 
 * @returns {Promise<string>}
 */
function readSSRCache(url) {
    let file_name = encodeURIComponent(url)
    return readFileFromCache(file_name)
}
function writeHTTPConfig(socket_port, http_port, shareLAN = false) {
    let cfg = []
    cfg.push(`forward-socks5 	/ 		127.0.0.1:${socket_port}  .`)
    cfg.push("forward         192.168.*.*/     		.")
    cfg.push("forward         10.*.*.*/        		.")
    cfg.push("forward         127.*.*.*/       		.")
    cfg.push(`listen-address ${shareLAN ? "0.0.0.0" : "127.0.0.1"}:${http_port}`)
    log(`${SSRPROXY}privoxy config:${cfg.join('\n')}`)
    return writeFileToCache(Share.Names.PRIVOXY_CONFIG_FILE_NAME, cfg.join('\n'))
}
/**
 * 
 * @param {string} file_name 
 * @param {string} content 
 */
function writeFileToCache(file_name, content) {
    return new Promise((resolve) => {
        let content_byte = new GLib.Bytes(content)
        GLib.mkdir_with_parents(Share.Path.CACHE_DIR, parseInt('0775', 8))
        let file = Gio.File.new_for_path(Share.Path.CACHE_DIR + '/' + file_name)
        file.replace_async(null, false, Gio.FileCreateFlags.NONE, GLib.PRIORITY_DEFAULT, null, (source_obj, res) => {
            let stream = source_obj.replace_finish(res);
            stream.write_bytes_async(content_byte, GLib.PRIORITY_DEFAULT,
                null, function (w_obj, w_res) {
                    w_obj.write_bytes_finish(w_res);
                    stream.close(null);
                    resolve()
                });
        })
    })
}
function readFileFromCache(file_name) {
    return new Promise((resolve) => {
        let full_path = Share.Path.CACHE_DIR + '/' + file_name
        if (GLib.file_test(full_path, FileTest.EXISTS)) {
            let file = Gio.File.new_for_path(full_path);
            file.load_contents_async(null, function (obj, res) {
                let [success, contents] = obj.load_contents_finish(res);
                if (success) {
                    resolve(ByteArray.toString(contents))
                }
                else {
                    resolve(null);
                }

            });
        } else {
            resolve(null)
        }
    })
}